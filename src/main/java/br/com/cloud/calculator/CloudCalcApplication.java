package br.com.cloud.calculator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The type Cloud calc application.
 */
@SpringBootApplication
public class CloudCalcApplication {

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {

        SpringApplication.run(CloudCalcApplication.class, args);
    }

}
