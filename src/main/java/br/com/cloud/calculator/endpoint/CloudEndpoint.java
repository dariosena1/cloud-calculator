package br.com.cloud.calculator.endpoint;

import br.com.cloud.calculator.repository.CloudRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("endpoint/v1/cloud")
public class CloudEndpoint {

    private final CloudRepository cloudRepository;

    @Autowired
    public CloudEndpoint(CloudRepository cloudRepository) {
        this.cloudRepository = cloudRepository;
    }


}
