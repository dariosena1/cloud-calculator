package br.com.cloud.calculator.model;

import java.util.UUID;

/**
 * The type Cloud.
 */
public class Cloud extends BaseEntity {

    private String name;

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }
}
