package br.com.cloud.calculator.repository;

import br.com.cloud.calculator.model.Cloud;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.UUID;

public interface CloudRepository extends CrudRepository<Cloud, UUID> {
    List<Cloud> findByName(String name);
}
