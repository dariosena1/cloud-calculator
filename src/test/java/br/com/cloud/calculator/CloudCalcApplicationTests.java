package br.com.cloud.calculator;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * The type Cloud calc application tests.
 */
@SpringBootTest
class CloudCalcApplicationTests {

	/**
	 * Context loads.
	 */
	@Test
	void contextLoads() {
	}

}
